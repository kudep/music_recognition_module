#!/bin/bash


index=0

name_sound_file[1]="пять_минут.mp3"
name_achiv_file[1]="5min_noise.tar"


vop_ar=${#name_sound_file[*]}

while [ "$index" -lt "$vop_ar" ]
do
index=`expr $index + 1`
echo "${name_sound_file[index]}      ${name_achiv_file[index]}"

mplayer "${name_sound_file[index]}"
sleep 65
tar -cf "${name_achiv_file[index]}" *.log *.wav
rm -v hash* record* flag.log
echo "" > report.log
sleep 15
done
