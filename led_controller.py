import logging
import Adafruit_BBIO.GPIO as GPIO
#logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.DEBUG)

class led_controller:



    #_pin_enable = False
    #_pin_disable = True
    _pin_enable = GPIO.HIGH
    _pin_disable = GPIO.LOW
    _led_pins={'power':'P8_8','connect':'P8_7','recognitionR':'P8_9','recognitionB':'P8_10'}
    _power_state=False
    _connect_state=False
    _recognition_state=0
    _update_flag=False

    def __init__(self,states_status=[False,False,0],led_pins={}):
        for key,value in led_pins:
            self._led_pins[key]=value
        for value in self._led_pins.values():
            logging.debug( u'Used pin ' +str(value))
            GPIO.setup(value, GPIO.OUT)
            GPIO.output(value, self._pin_disable)
        self._power_state=states_status[0]
        self._connect_state=states_status[1]
        self._recognition_state=states_status[2]
        self._update_flag=True

    def power_set(self,state=True):
        self._power_state=state
        self._update_flag=True
        
    def connect_set(self,state=True):
        self._connect_state=state
        self._update_flag=True
        
    def recognition_set(self,state=3):
        self._recognition_state=state
        self._update_flag=True
        
    def power_get(self):
        return self._power_state
        
    def connect_get(self):
        return self._connect_state
        
    def recognition_get(self):
        return self.ecognition_state
        
    def update(self):
        if self._update_flag==True:
            self._update_flag=False
            if self._power_state:
                self._GPIOoutput('power', self._pin_enable)
            else:
                self._GPIOoutput('power', self._pin_disable)
            
            if self._connect_state:
                self._GPIOoutput('connect', self._pin_enable)
            else:
                self._GPIOoutput('connect', self._pin_disable)
    
            if self._recognition_state==0:
                self._GPIOoutput('recognitionR', self._pin_disable)
                self._GPIOoutput('recognitionB', self._pin_disable)
    
            elif self._recognition_state==1:
                self._GPIOoutput('recognitionR', self._pin_enable)
                self._GPIOoutput('recognitionB', self._pin_disable)
            
            elif self._recognition_state==2:
                self._GPIOoutput('recognitionR', self._pin_disable)
                self._GPIOoutput('recognitionB', self._pin_enable)

    def _GPIOoutput(self,pin_name='power', value=True):
        if value==self._pin_enable:
            logging.debug( pin_name + u' on, enable pin ' + str(self._led_pins[pin_name]))
        else:
            logging.debug( pin_name + u' off, disable pin ' + str(self._led_pins[pin_name]))
        GPIO.output(self._led_pins[pin_name], value)

"""
led_cntrl=led_controller()
led_cntrl.update()
led_cntrl.recognition_set()
led_cntrl.power_set()
led_cntrl.connect_set()
led_cntrl.update()
"""