#!/usr/bin/python
#Base sector

import logging
import led_controller as l_c
import os.path
import time
#it's time delay to wait to SD-card init is ready to be wrote
time.sleep(30)
#Constants:
log_file_name=u'/media/log/quinta.log'
alternative_log_file_name=u'/var/log/quinta.log'
if(os.path.exists("/media/log")):
    logging.basicConfig(format = u'# %(levelname)-8s [%(asctime)s] (%(threadName)-10s) %(message)s', level = logging.INFO, filename = log_file_name)#, filename = log_file_name)
else:
    logging.basicConfig(format = u'# %(levelname)-8s [%(asctime)s] (%(threadName)-10s) %(message)s', level = logging.INFO, filename = alternative_log_file_name)#, filename = log_file_name)

#log_file_name=u'report.log'



def load(in_file_name = u'file',mode = u'rb'):
    logging.debug( u'Wait to load by file '+ in_file_name)
    try:
        in_file = open(in_file_name, mode)
        buf = in_file.read()
    except IOError as msg:
        logging.error('Could not open file. {0}'.format(msg))
        return
    except TypeError as msg:
        logging.error('Could not open file. {0}'.format(msg))
        return
    in_file.close()
    logging.debug( u'Complited')
    return buf

def save(buf,out_file_name = u'file',mode = u'wb'):
    logging.debug( u'Wait to save to file '+ out_file_name)
    out_file = open(out_file_name, mode)
    out_file.write(buf)
    out_file.close()
    logging.debug( u'Complited')



#Load configuration sector
import json
import sys

def config_import(in_file_name=u'config.json'):
    logging.debug( u'Wait to import configuration '+ in_file_name)
    try:
        json_buf=json.loads(load(in_file_name))
        logging.debug( u'Complited')
    except AttributeError as msg:
        logging.error( u'Could not import configuration. Use default settings. {0}'.format(msg))
        logging.error(load(in_file_name))
        json_buf={}
    return json_buf

if len(sys.argv) == 2:
    config_data=config_import(sys.argv[1])
else:
    config_name=None
    logging.error( u'Could not import configuration. Please, set only path for as argument.')
    config_data={}

#Getter sector

import alsaaudio
import wave
import struct
import echoprint

#Constants:
chunk = 2048
audio_format = alsaaudio.PCM_FORMAT_S16_LE
channel = 1
setsampwidth=2 #the sample width to n bytes
device_rate = 44100 #Webcam 11025+11025//2 Device 44100
echoprint_rate = 11025
full_time = 60
record_seconds = 30
norm_const = 32768.0

volume_average_threshold=2000

file_name='hash.tmp'
device_name='Device'#Webcam Device

#For hash save
hash_name=u'hash'
hash_type_name=u'.log'
count_hash=0

#For hash save
wav_file_name=u'record'
wav_file_type_name=u'.wav'
count_wav_file=0
samples_list=[]

samples_list=[list() for x in xrange(full_time/record_seconds)]

def samples_queue(samples=""):
    samples_list.pop(0)
    samples_list.append(samples)
    all_samples=list()
    for samples in samples_list:
        all_samples+=samples
    return all_samples

def device_list():
    card_list = {}
    report=u''
    for device_number, card_name in enumerate(alsaaudio.cards()):
        card_list[card_name] = 'hw:%s,0' % device_number
        report =report+card_name+' - '+card_list[card_name]+', '
    logging.debug( u'Available devices: ' +report[:-2] )
    return card_list


def music_volume_analyzer(buff=[]):

    max_samples = []
    max_average_power=record_seconds
    max_average_len=len(buff)/max_average_power
    for i in range(0, len(buff),len(buff)/max_average_power):
        max_samples.append(abs(max(buff[i:i+max_average_power])))
    max_value,max_samples_average=max(max_samples)*norm_const,sum(max_samples)/len(max_samples)*norm_const
    return max_value,max_samples_average

def getter_sound_with_save(card_list,device_name=u'Device'):

    logging.debug( u'Used device is ' + device_name)
    logging.debug( u'Start record')
    inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, card_list[device_name])
    inp.setchannels(channel)
    inp.setrate(device_rate)
    inp.setformat(audio_format)
    inp.setperiodsize(chunk)

    global count_wav_file
    count_wav_file=count_wav_file+1

    w = wave.open(wav_file_name+str(count_wav_file).zfill(3)+wav_file_type_name, 'w')
    w.setnchannels(channel)
    w.setsampwidth(setsampwidth)
    w.setframerate(device_rate)

    samples = []
    divider=device_rate/echoprint_rate
    frames_of_second=device_rate / chunk
    for i in range(0, frames_of_second * record_seconds):
        l, data = inp.read()
        for i in range(0, len(data),setsampwidth*divider):
            samples.append(struct.unpack('h', data[i:i+2])[0]/norm_const)
        w.writeframes(data)
    w.close()

    d = echoprint.codegen(samples_queue(samples), 0)
    #d = echoprint.codegen(samples, 0)
    max_value,samples_average=music_volume_analyzer(samples)
    logging.debug( u'Complited.')
    return d['code'],max_value,samples_average
blink_flag=False
def blink(flag):
    global blink_flag
    if(flag):
        blink_flag= not blink_flag
        led_cntrl.power_set(blink_flag)
        led_cntrl.update()

def getter_sound(card_list,device_name='Device'):

    logging.debug( u'Used device is ' + device_name)
    logging.debug( u'Start record')
    inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, card_list[device_name])
    inp.setchannels(channel)
    inp.setrate(device_rate)
    inp.setformat(audio_format)
    inp.setperiodsize(chunk)

    samples = []
    divider=device_rate/echoprint_rate
    frames_of_second=device_rate / chunk
    for i in xrange(0, frames_of_second * record_seconds):
        l, data = inp.read()
        blink(divmod(i,frames_of_second/2)[1]==0)
        for i in range(0, len(data),setsampwidth*divider):
            try:
                samples.append(struct.unpack('h', data[i:i+2])[0]/norm_const)
            except struct.error as msg:
                logging.error( u'Unexpected error: {0}. Perhaps there: unpack requires a string argument of length 2. But it can be needed to check.'.format(msg))
            except:
                logging.error( u'Unexpected error: {0}. Perhaps there: unpack requires a string argument of length 2. But it can be needed to check.'.format(sys.exc_info()[0]))

    d = echoprint.codegen(samples_queue(samples), 0)
    max_value,samples_average=music_volume_analyzer(samples)
    logging.debug( u'Complited.')
    return d['code'],max_value,samples_average

def hash_save(hash):
    global count_hash
    count_hash=count_hash+1
    save(hash,hash_name+str(count_hash).zfill(3)+hash_type_name)


#Protobuf sector
import kvinta3_pb2
import hashlib


#Constants:


def login(packet,save_login_name='login',password='key',message_id=1,device_id=99):
    packet.packet_type=kvinta3_pb2.Packet.LOGIN
    packet.login_message.hash_key=hashlib.sha1(password).hexdigest()
    packet.login_message.header.message_id=message_id
    packet.login_message.header.sender_id=device_id


    return packet.SerializeToString()


def hash_to_data(packet,hash_buf,message_id=1,device_id=99):
    logging.debug( u'Wait to convert hash to data.')
    packet.packet_type=kvinta3_pb2.Packet.DATA
    packet.data_message.header.message_id=message_id
    packet.data_message.header.sender_id=device_id

    packet.data_message.data=hash_buf

    logging.debug( u'Size the data: '+str(packet.ByteSize())+ ' bytes.')
    return packet.SerializeToString()


def resp_parser(packet,buf):
    logging.debug( u'Wait to parse.')
    try:
        packet.ParseFromString(buf)
        logging.debug( u'Complited.')
    except:
        logging.error( u'Could not parse a resived buffer because it is damaged.')
    return packet


def get_data_results(packet):
    logging.debug( u'Wait to get results.')
    result = packet.data_response.result
    status = packet.data_response.status
    score = packet.data_response.score
    logging.debug( u'Complited.')
    return (result,status,score)


#TCP sector

import google.protobuf.internal.encoder as encoder
import google.protobuf.internal.decoder as decoder
import socket
#Constants:

max_size=99999999
shear= 10
connect_repeat_time= 10
tcp_socket = None
timeout= 60
addr=("localhost",8080)


def refresh_buffer(buf, value, max_count):
    if(len(buf)<max_count):
        buf.append(value)
        logging.debug('Queue size {0}'.format(len(buf)))
    else:
        logging.debug('Overflow queue')
        buf.pop(0)
        buf.append(value)



def _connect():
    logging.debug( u'Wait to connect to ip: '+addr[0]+' port: '+str(addr[1])+'.')
    global tcp_socket
    while True:
        try:
            tcp_socket = socket.socket()
            tcp_socket.connect(addr)
            tcp_socket.settimeout(timeout)
            logging.debug( u'Complited.' )
            break
        except socket.error as msg:
            refresh_buffer(led_buffer,False,max_led_buffer_num)
            logging.error( u'Could not connect. ' + str(msg))
            time.sleep(connect_repeat_time) # delays for connect_repeat_time seconds


def connect(host = 'recognize.quinta.online',port = 12332,_timeout = 20):
    global addr
    addr = (host,port)
    global timeout
    timeout=_timeout
    _connect()


def disconnect():
    logging.debug( u'Wait to disconnect.')
    tcp_socket.close()
    logging.debug( u'Complited.')

def send(buf):
    logging.debug( u'Wait to send.')
    try:
        buf = encoder._VarintBytes(len(buf)) + buf
        tcp_socket.send(buf)
        logging.debug( u'Complited.')
    except socket.error as msg:
        disconnect()
        refresh_buffer(led_buffer,False,max_led_buffer_num)
        logging.error( u'Could not recive. ' + str(msg))
        _connect()
        return


def recive():
    logging.debug( u'Wait to recive.')
    try:
        buf = tcp_socket.recv(max_size)
        (size, position) = decoder._DecodeVarint(buf, 0)
        logging.debug( u'Complited.')
        logging.debug( u'Recived: '+str(size)+ ' bytes.')
        logging.debug( 'Message end is ' + str(buf[position+size-shear:position+size]))
        return buf[position:position+size]
    except socket.error as msg:
        disconnect()
        refresh_buffer(led_buffer,False,max_led_buffer_num)
        logging.error( u'Could not recive. ' + str(msg))
        _connect()
        return
    except IndexError as msg:
        disconnect()
        refresh_buffer(led_buffer,False,max_led_buffer_num)
        logging.error( u'Could not recive. ' + str(msg))
        _connect()
        return



#Service sector

import os.path

flag_file_name='flag.log'

resuls=[False,False,False,False]

magnitude_average_threshold = 150

def result_statistics(resul=False,samples_average=0):
    resuls.pop(0)
    resuls.append(resul)
    if(samples_average>magnitude_average_threshold):
        if resul :
            return 2
        else:
            return 1
    else:
        return 0

silences=[False,False,False,False]

def silence_statistics(silence=False):
    silences.pop(0)
    silences.append(silence)
    return sum(silences)==0

def next_track(flag_file_name):
    global count_hash
    global count_wav_file
    if not os.path.exists(flag_file_name):
        save("flag",flag_file_name)
        count_hash=0
        count_wav_file=0
        return True
    return False



#Thread sector

import threading


#Hash thread

led_buffer=[]
hash_buffer=[]
max_client_buffer_num=10# size = max_client_buffer_num*3kB
def hash_thread(buf):
    """thread hash function"""
    operation_num=0
    while True:
        operation_num+=1;
        card_list=device_list()
        data=(getter_sound(card_list,device_name),operation_num)
        refresh_buffer(buf,data,max_client_buffer_num)
        logging.info('Complited hash operation number {0}'.format(operation_num))

client_thread_sleep_time=2
max_led_buffer_num=2
def client_thread(buf,led_buf):
    """thread client function"""
    connect()
    login_packet=login(kvinta3_pb2.Packet(),device_id=config_data.get("device_id",13))
    send(login_packet)
    login_resp_packet=recive()
    led_cntrl.connect_set()
    led_cntrl.update()
    while True:
        while len(buf)==0:
            time.sleep(client_thread_sleep_time)

        #Get values from queue
        ((hash,max_value,samples_average),operation_num)= buf.pop(0)

        #Packet hash
        data_packet=hash_to_data(kvinta3_pb2.Packet(),hash)

        #Send and recive data
        send(data_packet)
        recive_buf=recive()
        while recive_buf==None:
            refresh_buffer(led_buf,False,max_led_buffer_num)
            login_packet=login(kvinta3_pb2.Packet())
            send(login_packet)
            login_resp_packet=recive()
            send(data_packet)
            recive_buf=recive()

        refresh_buffer(led_buf,True,max_led_buffer_num)
        #Unpacket result
        data_resp_packet=resp_parser(kvinta3_pb2.Packet(),recive_buf)
        (result,status,score) =get_data_results(data_resp_packet)

        #Publish the result
        logging.info('Result: {0}, Status: {1}, Score: {2}, Max: {3}, Average: {4}'.format(result,status,int(score),int(max_value),int(samples_average)))
        logging.info('Complited dispatch data number {0}'.format(operation_num))
        led_cntrl.recognition_set(result_statistics(result,samples_average))
        led_cntrl.update()



led_thread_sleep_time=2
led_complited_time=0.2
led_complited_repeat=10
def led_thread(buf):
    """thread led function"""
    while True:
        while len(buf)==0:
            time.sleep(led_thread_sleep_time)
        if(buf.pop(0)):
            for i in xrange(led_complited_repeat):
                led_cntrl.connect_set()
                led_cntrl.update()
                time.sleep(led_complited_time)
                led_cntrl.connect_set(False)
                led_cntrl.update()
                time.sleep(led_complited_time)
            led_cntrl.connect_set()
            led_cntrl.update()
        else:
            led_cntrl.connect_set(False)
            led_cntrl.update()


#Init


#Start program
led_cntrl=l_c.led_controller()

threads = []
t = threading.Thread(name='Hasher',target=hash_thread, args=(hash_buffer,))
threads.append(t)
t.start()
t = threading.Thread(name='Client',target=client_thread, args=(hash_buffer,led_buffer,))
threads.append(t)
t.start()
t = threading.Thread(name='Led',target=led_thread, args=(led_buffer,))
threads.append(t)
t.start()

